; <?php/*

[general]
name							="LibPHPExcel"
version							="1.0.0"
encoding						="UTF-8"
description						="Library based on PHPExcel library used to edit excel files."
description.fr					="Bibliothèque basé sur PHPExcel servant à editer des fichiers excel."
delete							=1
ov_version						="8.4.0"
php_version						="5.2.0"
mysql_version					="4.1.2"
addon_access_control			=0
author							="Cantico"
icon							="icon.png"
mysql_character_set_database	="latin1,utf8"

mod_xml							="Available"
mod_gd2							="Available"
mod_zip							="Available"
;*/?>